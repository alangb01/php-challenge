<?php

namespace Tests;

use App\Carriers\Carrier;
use App\Carriers\CarrierOne;
use App\Carriers\TwilioCarrier;
use App\Mobile;
use PHPUnit\Framework\TestCase;

class MobileTest extends TestCase
{
	
	/** @test */
	public function it_returns_null_when_name_empty()
	{
        $provider=new Carrier();
        $mobile = new Mobile($provider);
		$this->assertNull($mobile->makeCallByName(''));
	}

    /** @test */
    public function it_returns_call_when_name_is_not_empty()
    {
        $provider=new Carrier();
        $mobile = new Mobile($provider);

        $this->assertInstanceOf(\App\Call::class,$mobile->makeCallByName('Victoria'));
    }

    /** @test */
    public function it_returns_exception_when_contact_is_not_found()
    {
        $this->expectException(\Exception::class);

        $provider=new Carrier();
        $mobile = new Mobile($provider);

        $mobile->makeCallByName('Pamela');
    }

    /** @test */
    public function it_returns_exception_when_phone_is_not_valid()
    {
        $this->expectException(\Exception::class);
        $provider=new Carrier();
        $mobile = new Mobile($provider);

        $mobile->makeSMSByName('Invalid',"Hola como estas");

    }

    /** @test */
    public function it_returns_null_when_name_or_body_is_empty()
    {
        $this->expectException(\Exception::class);
        $provider=new Carrier();
        $mobile = new Mobile($provider);

        $mobile->makeSMSByName('','body');

    }

    /** @test */
    public function it_returns_null_when_body_or_body_is_empty()
    {
        $this->expectException(\Exception::class);
        $provider=new Carrier();
        $mobile = new Mobile($provider);

        $mobile->makeSMSByName('body','');

    }

    /** @test */
    public function it_returns_message_when_contact_is_valid()
    {
        $provider=new Carrier();
        $mobile = new Mobile($provider);

        $this->assertInstanceOf(\App\Message::class,$mobile->makeSMSByName('Victoria','Hola todo ok'));
    }

    /** @test */
    public function twilio_send_sms(){
        $provider=new Carrier();
        $mobile=new Mobile($provider);

        $mobile->changeCarrier(new TwilioCarrier());

        $this->assertInstanceOf(\App\Message::class,$mobile->makeSMSByName('Alan','Hola todo ok'));
    }
}
