<?php

namespace App\Carriers;

use App\Call;
use App\Contact;
use App\Interfaces\CarrierInterface;
use App\Message;

class Carrier implements CarrierInterface
{
    private $contact;
    private $body;

    public function dialContact(Contact $contact)
    {
        // TODO: Implement dialContact() method.
        $this->contact=$contact;
    }

    public function makeCall(): Call
    {
        // TODO: Implement makeCall() method.
        return new Call($this->contact->getPhone());
    }

    public function prepareSMS($body)
    {
        $this->body=$body;
    }

    public function sendSMS( ): Message
    {
        // TODO: Implement makeSMS() method.
        return new Message($this->contact->getPhone(),$this->body);
    }
}