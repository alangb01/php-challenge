<?php

namespace App\Carriers;

use App\Call;
use App\Contact;
use App\Interfaces\CarrierInterface;
use App\Message;
use Twilio\Rest\Client;

class TwilioCarrier implements CarrierInterface
{
    private $contact;
    private $body;

    private $number_from='+15017122661';

    public function dialContact(Contact $contact)
    {
        // TODO: Implement dialContact() method.
        $this->contact=$contact;
    }

    public function makeCall(): Call
    {
        // TODO: Implement makeCall() method.
        return new Call($this->contact->getPhone());
    }

    public function prepareSMS($body)
    {
        $this->body=$body;
    }

    public function sendSMS( ): Message
    {
        // TODO: Implement makeSMS() method.

// Your Account SID and Auth Token from twilio.com/console
        $sid = 'ACd4cfbc53cef1ed4ba3eb42900b3e3030';
        $token = '28b0547794d09dbedd76a34e52aad89c';
        $client = new Client($sid, $token);

// Use the client to do fun stuff like send text messages!
        $client->messages->create(
        // the number you'd like to send the message to
            '+51'.$this->contact->getPhone(),
            [
                // A Twilio phone number you purchased at twilio.com/console
                'from' =>$this->number_from,
                // the body of the text message you'd like to send
                'body' => $this->body
            ]
        );

        return new Message($this->contact->getPhone(),$this->body);
    }
}