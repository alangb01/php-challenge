<?php

namespace App;


class Contact
{
    private $name;
    private $phone;

    function __construct($name,$phone)
    {
        $this->name=$name;
        $this->phone=$phone;
    }

    /**
     * @return mixed
     */
    public function getName(){
        return $this->name;
    }



    /**
     * @param mixed $firstName
     */
    public function getPhone(){
        return $this->phone;
    }


}