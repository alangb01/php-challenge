<?php

namespace App\Services;

use App\Contact;
use ErrorException;


class ContactService
{
	private static function generateContacts(){
        $list=array();
        $list[]=new Contact("Alan","Gastelo","911111111");
        $list[]=new Contact("Barbara","Bohorquez","911111112");
        $list[]=new Contact("Pamela","Gonzales","911111113");
        $list[]=new Contact("Martin","Fiestas","252525");

        return $list;
    }


    /**
     * @Return Contact
     * @throws \Exception
     */
    public static function findByName(string $name): Contact
	{
		// queries to the db
        $contactos=array(
            new Contact('Alan',978772143),
            new Contact('Victoria',931978080),
            new Contact('Chicho',99999999),
            new Contact('Invalid',"sin numero")
        );

        foreach($contactos as $contact){
            if($contact->getName()==$name){
                return $contact;
            }
        }

        throw new \Exception('Contact not found');
	}

	public static function validateNumber(string $number): bool
	{
		// logic to validate numbers
        //peruvian numbers
        return strlen($number)==9;
	}
}