<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;


class Mobile
{

	protected $provider;
	
	function __construct(CarrierInterface $provider)
	{
		$this->provider = $provider;
	}


    function changeCarrier(CarrierInterface $provider){
        $this->provider=$provider;
    }


	public function makeCallByName($name = '')
	{
		if( empty($name) ) return;

        $contact = ContactService::findByName($name);

        $this->provider->dialContact($contact);

        return $this->provider->makeCall();
	}
    public function makeSMSByName($name='',$body=''){
        if( empty($name) ) throw New \Exception("Debe ingresar el nombre del contacto");
        if( empty($body) ) throw New \Exception("Debe ingresar el mensaje");

        $contact = ContactService::findByName($name);

        if(!ContactService::validateNumber($contact->getPhone())) throw new \Exception("Invalid number");

        $this->provider->dialContact($contact);
        $this->provider->prepareSMS($body);

        return $this->provider->sendSMS();
    }
}
